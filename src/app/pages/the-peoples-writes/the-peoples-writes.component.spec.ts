import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThePeoplesWritesComponent } from './the-peoples-writes.component';

describe('ThePeoplesWritesComponent', () => {
  let component: ThePeoplesWritesComponent;
  let fixture: ComponentFixture<ThePeoplesWritesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThePeoplesWritesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThePeoplesWritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
