import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { SwiperComponent } from 'swiper/angular';
import { DatePickerComponent } from 'ng2-date-picker';
import { SignaturePad } from 'angular2-signaturepad';

// import Swiper core and required components
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller,
} from 'swiper/core';

import { ContactService } from '../../services/contact.service';

// install Swiper components
SwiperCore.use([
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller,
]);

@Component({
  selector: 'app-home2',
  templateUrl: './home2.component.html',
  styleUrls: ['./home2.component.scss'],
})
export class Home2Component implements OnInit, AfterViewInit {
  @ViewChild('dayPicker') datePicker: DatePickerComponent;
  title = 'Angular signature example';
  signatureImg: string;
  signatureImg2: boolean = false;
  freemealsImg: string;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  signaturePadOptions: Object = {
    minWidth: 2,
    canvasWidth: 300,
    canvasHeight: 300,
  };

  public slidesStore: any[];
  public slidesStoreDesktop: any[];
  public slides2 = ['slide 1', 'slide 2', 'slide 3'];

  personalDetails!: FormGroup;
  datesAttendingDetails: FormGroup;
  parentCarerDetails: FormGroup;
  contactDetails: FormGroup;
  altContactDetails: FormGroup;
  medicalDetails: FormGroup;
  additionalDetails: FormGroup;
  signatureDetails: FormGroup;


  public filePath: string;
  public applicationData: any;

  personal_step = false;
  datesAttending_step = false;
  parentCarer_step = false;
  contact_step = false;
  altContact_step = false;
  medical_step = false;
  additional_step = false;
  signature_step = false;
  step = 1;
  sig = true;

  constructor(
    private formBuilder: FormBuilder,
    private contactServices: ContactService
  ) {
    this.slidesStore = [
      {
        id: '001',
        src: './assets/images/dorothee-play.jpg',
        mobilesrc: './assets/images/dorothee-play_mobile.jpg',
        alt: 'IN THE 18TH CENTURY',
        title: 'IN THE 18TH CENTURY',
        CTALabel: 'Dorothee',
        CTALabelDesktop: 'INFORMATION',
        CTALink: '#one',
        Theme: "dorotheeBtn",
      },      
      {
        id: '002',
        src: './assets/images/samuel-mudian-slide.jpg',
        mobilesrc: './assets/images/samuel-mudian-slide_mobile.jpg',
        alt: 'Samuel Mudian',
        title: 'Samuel Mudian',
        CTALabel: 'Samuel Mudian',
        CTALabelDesktop: 'INFORMATION',
        CTALink: '#two',
        Theme: "samuelBtn",
      },
      {
        id: '003',
        src: './assets/images/john-slide.jpg',
        mobilesrc: './assets/images/john-slide_mobile.jpg',
        alt: 'Revisualising John',
        title: 'Revisualising John',
        CTALabel: 'John',
        CTALabelDesktop: 'INFORMATION',
        CTALink: '#five',
        Theme: "johnBtn",
      },      
      {
        id: '004',
        src: './assets/images/samuelrevis-slide.jpg',
        mobilesrc: './assets/images/samuelrevis-slide_mobile.jpg',
        alt: 'Revisualising Samuel Linley',
        title: 'Revisualising Samuel Linley',
        CTALabel: 'Samuel Linley',
        CTALabelDesktop: 'INFORMATION',
        CTALink: '#four',
        Theme: "linleyBtn",
      },
      {
        id: '005',
        src: './assets/images/charles-caesar-slider.jpg',
        mobilesrc: './assets/images/charles-caesar-slider_mobile.jpg',
        alt: 'Charles Caesar Visualised',
        title: 'Charles Caesar Visualised',
        CTALabel: 'Charles Caesar',
        CTALabelDesktop: 'INFORMATION',
        CTALink: '#three',
        Theme: "charlesBtn",
      },
      {
        id: '006',
        src: './assets/images/thomas-slider.jpg',
        mobilesrc: './assets/images/thomas-slider_mobile.jpg',
        alt: 'Thomas Visualised',
        title: 'Thomas Visualised',
        CTALabel: 'Thomas Visualised',
        CTALabelDesktop: 'INFORMATION',
        CTALink: '#six',
        Theme: "thomasBtn",
      },/*
      {
        id: '007',
        src: './assets/images/dorotheevis_slider.jpg',
        mobilesrc: './assets/images/dorotheevis_slider_mobile.jpg',
        alt: 'Dorothee Visualised',
        title: 'Dorothee Visualised',
        CTALabel: 'Dorothee Visualised',
        CTALabelDesktop: 'INFORMATION',
        CTALink: '#seven',
        Theme: "dorotheevisBtn",
      },*/
      {
        id: '008',
        src: './assets/images/home1.jpg',
        mobilesrc: './assets/images/home1_mobile.jpg',
        alt: 'About Us',
        title: 'About Us',
        CTALabel: 'ABOUT US',
        CTALabelDesktop: 'ABOUT US',        
        CTALink: '/aboutus',
        Theme: "thomasBtn",
      },
      {
        id: '009',
        src: './assets/images/dorothee20.jpg',
        mobilesrc: './assets/images/dorothee20_mobile.jpg',
        alt: 'PLAY YOUR PART',
        title: 'PLAY YOUR PART',
        CTALabel: 'PLAY YOUR PART',
        CTALabelDesktop: 'PLAY YOUR PART',  
        CTALink: '/the-peoples-writes#dorothee20',
        Theme: "thomasBtn",
      },
    ];
    this.slidesStoreDesktop = [
      {
        id: '001',
        src: './assets/images/dorothee-play.jpg',
        mobilesrc: './assets/images/dorothee-play_mobile.jpg',
        alt: 'IN THE 18TH CENTURY',
        title: 'IN THE 18TH CENTURY',
        CTALabel: 'Dorothee',
        CTALabelDesktop: 'INFORMATION',
        CTALink: '#one',
        Theme: "dorotheeBtn",
      },      
      {
        id: '002',
        src: './assets/images/futureBG.jpg',
        mobilesrc: './assets/images/samuel-mudian-slide_mobile.jpg',
        alt: 'Samuel Mudian',
        title: 'Samuel Mudian',
        CTALabel: 'Samuel Mudian',
        CTALabelDesktop: 'INFORMATION',
        CTALink: '#two',
        Theme: "samuelBtn",
      },
      {
        id: '008',
        src: './assets/images/home1.jpg',
        mobilesrc: './assets/images/home1_mobile.jpg',
        alt: 'About Us',
        title: 'About Us',
        CTALabel: 'ABOUT US',
        CTALabelDesktop: 'ABOUT US',        
        CTALink: '/aboutus',
        Theme: "thomasBtn",
      },
      {
        id: '009',
        src: './assets/images/dorothee20.jpg',
        mobilesrc: './assets/images/dorothee20_mobile.jpg',
        alt: 'PLAY YOUR PART',
        title: 'PLAY YOUR PART',
        CTALabel: 'PLAY YOUR PART',
        CTALabelDesktop: 'PLAY YOUR PART',  
        CTALink: '/the-peoples-writes#dorothee20',
        Theme: "thomasBtn",
      },
    ];    
  }



  
  open() {
    this.datePicker.api.open();
  }
  close() {
    this.datePicker.api.close();
  }

  onSwiper(swiper) {
    //console.log(swiper);
  }
  onSlideChange() {
    //console.log('slide change');
  }

  ngOnInit(): void {
    var acc = document.getElementsByClassName('accordion');
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener('click', function () {
        var allPanels = document.querySelectorAll('.panel');
        //console.log('allPanels', allPanels)

        for (var ii = 0; ii < allPanels.length; ii++) {
          allPanels[ii].setAttribute('style', 'max-heght:0');
          if (ii !== i) this.classList.toggle('active');
          //acc[i]
          //console.log(
          //  "allPanels[ii].getAttribute('style')",
          //  allPanels[ii].getAttribute('style')
          //);
        }

        this.classList.toggle('active');

        var panel = this.nextElementSibling;
        //console.log('panel.style.maxHeight', panel.style);
        if (panel.style.maxHeight === '0px' || panel.style.maxHeight === '') {
          panel.style.maxHeight = '5000px';
        } else {
          panel.style.maxHeight = '0px';
        }
      });
    }

    this.personalDetails = this.formBuilder.group({
      //1
      name: ['', Validators.required],
      dob: ['', Validators.required],
      address: ['', Validators.required],
    });

    this.datesAttendingDetails = this.formBuilder.group({
      //2
      alldates: [''],
      monday16th: [''],
      tuesday17th: [''],
      wednesday18th: [''],
      thursday19th: [''],
      frisday20th: [''],
    });

    this.parentCarerDetails = this.formBuilder.group({
      //3
      parentCarerName: ['', Validators.required],
      relationshipToYP: ['', Validators.required],
      parentCarerAddress: ['', Validators.required],
      parentCarerHomeTel: ['', Validators.required],
      parentCarerMobileTel: ['', Validators.required],
      parentCarerEmail: ['', Validators.required],
      parentFreeMeals: [''],
      img: [null],
    });

    this.contactDetails = this.formBuilder.group({
      //4
      homeTel: [''],
      mobileTel: [''],
      email: [''],
    });

    this.altContactDetails = this.formBuilder.group({
      //5
      altCarerHomeTel: [''],
      altCarerMobileTel: [''],
      altCarerEmail: [''],
    });

    this.medicalDetails = this.formBuilder.group({
      //6
      asthmaBrochitis: [''],
      allergies: [''],
      ADHD: [''],
      Diabetes: [''],
      HCFFB: [''],
      hayfever: [''],
      headaches: [''],
      currentMT: [''],
      other: [''],
    });

    this.additionalDetails = this.formBuilder.group({
      //7
      diet: [''],
      disibility: [''],
      familyDoctor: [''],
      familyDoctorTel: [''],
      freeMealEvidence: [''], //upload
    });

    this.signatureDetails = this.formBuilder.group({
      //8
      signature: [''], //upload
    });
  }

  ngAfterViewInit() {
    // this.signaturePad is now available
    // this.signaturePad.set('minWidth', 2);
    // this.signaturePad.clear();
  }

  imagePreview(e) {
    const file = (e.target as HTMLInputElement).files[0];

    this.parentCarerDetails.patchValue({
      img: file
    });

    this.parentCarerDetails.get('img').updateValueAndValidity()

    const reader = new FileReader();
    reader.onload = () => {
      this.filePath = reader.result as string;
    }
    reader.readAsDataURL(file)
  }



  processing: boolean;

  onDragOver(event) {
    event.preventDefault();
  }

  onDropSuccess(event) {
    event.preventDefault();

    this.onFileChange(event.dataTransfer.files);
  }

  onChange(event) {
    this.onFileChange(event.target.files);
  }

  private onFileChange(files) {
    if (files.length) {
      this.processing = true;
      //console.log(`Processing ${files.length} file(s).`);
      setTimeout(() => {
        // Fake add attachment
        //console.log("Processed!", files);

        const file = files[0];

        this.parentCarerDetails.patchValue({
          img: file
        });
    
        this.parentCarerDetails.get('img').updateValueAndValidity()
    
        const reader = new FileReader();
        reader.onload = () => {
          this.filePath = reader.result as string;
          this.freemealsImg = reader.result as string;
        }
        reader.readAsDataURL(file)

        this.processing = false;
      }, 1000);
    }
  }


  drawComplete() {
    //console.log(this.signaturePad.toDataURL());
  }

  drawStart() {
    //console.log('begin drawing');
  }

  clearSignature() {
    // turn padd bak on turn off sig
    this.signaturePad.clear();
    //this.sig = false;
    //this.signatureImg = null;
    this.signatureImg2 = false;
  }

  savePad() {
    // turn off pad turn on sig
    //*ngIf="condition"
    this.sig = false;
    const base64Data = this.signaturePad.toDataURL();
    this.signatureImg = base64Data;
    this.signatureImg2 = true;
  }

  get personal() {
    //1
    return this.personalDetails.controls;
  }

  get datesAttending() {
    //2
    return this.datesAttendingDetails.controls;
  }

  get parentCarer() {
    //3
    return this.parentCarerDetails.controls;
  }

  get contact() {
    //4
    return this.contactDetails.controls;
  }

  get altContact() {
    //5
    return this.altContactDetails.controls;
  }

  get medical() {
    //6
    return this.medicalDetails.controls;
  }

  get additional() {
    //7
    return this.additionalDetails.controls;
  }

  get signature() {
    //8
    // this.signaturePad is now available
    this.signaturePad.set('minWidth', 2);
    this.signaturePad.clear();
    return this.signatureDetails.controls;
  }

  next() {
    if (this.step == 1) {
      this.personal_step = true;
      if (this.personalDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 2) {
      this.datesAttending_step = true;
      if (this.datesAttendingDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 3) {
      this.parentCarer_step = true;
      if (this.parentCarerDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 4) {
      this.contact_step = true;
      if (this.contactDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 5) {
      this.altContact_step = true;
      if (this.altContactDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 6) {
      this.medical_step = true;
      if (this.medicalDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 7) {
      this.additional_step = true;
      if (this.additionalDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 8) {
      this.signature_step = true;
      if (this.signatureDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
  }
  previous() {
    this.step--;
    if (this.step == 1) {
      this.personal_step = false;
    }
    if (this.step == 2) {
      this.datesAttending_step = false;
    }
    if (this.step == 3) {
      this.parentCarer_step = false;
    }
    if (this.step == 4) {
      this.contact_step = false;
    }
    if (this.step == 5) {
      this.altContact_step = false;
    }
    if (this.step == 6) {
      this.medical_step = false;
    }
    if (this.step == 7) {
      this.additional_step = false;
      this.signatureImg2 = false;
    }
  }
  submit() {
    if (this.step == 8) {
      this.signature_step = true;
      if (this.signatureDetails.invalid) {
        return;
      }
    }
  }
  onSubmit(): void {
    // collate all th formgroups data into one object
    this.applicationData = {
      ...this.personalDetails.value,
      ...this.datesAttendingDetails.value,
      ...this.parentCarerDetails.value,
      ...this.contactDetails.value,
      ...this.altContactDetails.value,
      ...this.medicalDetails.value,
      ...this.additionalDetails.value,
      ...this.signatureDetails.value,
    };
    this.contactServices.conactForm(this.applicationData, this.signaturePad, this.freemealsImg);
    //console.warn('Your order has been submitted', this.applicationData);
    alert('your application has been submitted');

    // reset all of the formgroups
    this.personalDetails.reset();
    this.datesAttendingDetails.reset();
    this.parentCarerDetails.reset();
    this.contactDetails.reset();
    this.altContactDetails.reset();
    this.medicalDetails.reset();
    this.additionalDetails.reset();
    this.signatureDetails.reset();
    this.step = 1;
  }
}
