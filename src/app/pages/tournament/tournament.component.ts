import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { DatePickerComponent } from 'ng2-date-picker';
import { SignaturePad } from 'angular2-signaturepad';
import { ContactService } from '../../services/contact.service';

@Component({
  selector: 'app-tournament',
  templateUrl: './tournament.component.html',
  styleUrls: ['./tournament.component.scss']
})
export class TournamentComponent  implements OnInit, AfterViewInit {

  personalDetails!: FormGroup;
  datesAttendingDetails: FormGroup;
  parentCarerDetails: FormGroup;
  contactDetails: FormGroup;
  altContactDetails: FormGroup;
  medicalDetails: FormGroup;
  dietryDetails: FormGroup;
  accessDetails: FormGroup;
  permissionDetails: FormGroup;
  signatureDetails: FormGroup;


  public filePath: string;
  public applicationData: any;

  personal_step = false;
  datesAttending_step = false;
  parentCarer_step = false;
  contact_step = false;
  altContact_step = false;
  medical_step = false;
  dietry_step = false;
  access_step = false;
  permission_step = false;
  signature_step = false;
  step = 1;
  sig = true;

  title = 'Angular signature example';
  signatureImg: string;
  signatureImg2: boolean = false;
  freemealsImg: string;

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  signaturePadOptions: Object = {
    minWidth: 2,
    canvasWidth: 300,
    canvasHeight: 300,
  };  

  constructor(
    private formBuilder: FormBuilder,
    private contactServices: ContactService
  ) { }

  ngOnInit(): void {

    this.personalDetails = this.formBuilder.group({
      //1
      name: ['', Validators.required],
      dob: ['', Validators.required],
      age: ['', Validators.required],
      nameofschool: ['', Validators.required],
    });

    this.datesAttendingDetails = this.formBuilder.group({
      //2
      monday20th: [''],
      tuesday21st: [''],
      wednesday22nd: [''],
      thursday23rd: [''],
    });

    this.parentCarerDetails = this.formBuilder.group({
      //3
      parentCarerName: ['', Validators.required],
      relationshipToYP: ['', Validators.required],
      parentCarerAddress: ['', Validators.required],
      parentCarerHomeTel: ['', Validators.required],
      parentCarerMobileTel: ['', Validators.required],
      parentCarerEmail: ['', Validators.required],
      parentFreeMeals: [''],
      img: [null],
    });

    this.contactDetails = this.formBuilder.group({
      //4
      homeTel: [''],
      workTel: [''],
      mobileTel: [''],
      email: [''],
    });

    /*
    this.altContactDetails = this.formBuilder.group({
      //5
      altCarerHomeTel: [''],
      altCarerMobileTel: [''],
      altCarerEmail: [''],
    });
    */

    this.medicalDetails = this.formBuilder.group({
      //5
      asthmaBrochitis: [''],
      allergies: [''],
      ADHD: [''],
      Diabetes: [''],
      HCFFB: [''],
      hayfever: [''],
      headaches: [''],
      currentMT: [''],
      other: [''],
    });

    this.dietryDetails = this.formBuilder.group({
      //6
      foodallergies: [''],
      veg: [''],
      foodreligious: [''],
      freeMealEvidence: [''], //upload
    });

    
    this.accessDetails = this.formBuilder.group({
      //7
      accessRequirement1: [''],
      accessRequirement2: [''],
    });
    
    this.permissionDetails = this.formBuilder.group({
      //8
      photoPermission: [''],
      processPermission: [''],
    });


    this.signatureDetails = this.formBuilder.group({
      //9
      signature: [''], //upload
    });
  }

  ngAfterViewInit() {
    // this.signaturePad is now available
    // this.signaturePad.set('minWidth', 2);
    // this.signaturePad.clear();
  }

  imagePreview(e) {
    const file = (e.target as HTMLInputElement).files[0];

    this.parentCarerDetails.patchValue({
      img: file
    });

    this.parentCarerDetails.get('img').updateValueAndValidity()

    const reader = new FileReader();
    reader.onload = () => {
      this.filePath = reader.result as string;
    }
    reader.readAsDataURL(file)
  }



  processing: boolean;

  onDragOver(event) {
    event.preventDefault();
  }

  onDropSuccess(event) {
    event.preventDefault();

    this.onFileChange(event.dataTransfer.files);
  }

  onChange(event) {
    this.onFileChange(event.target.files);
  }

  private onFileChange(files) {
    if (files.length) {
      this.processing = true;
      //console.log(`Processing ${files.length} file(s).`);
      setTimeout(() => {
        // Fake add attachment
        //console.log("Processed!", files);

        const file = files[0];

        this.parentCarerDetails.patchValue({
          img: file
        });
    
        this.parentCarerDetails.get('img').updateValueAndValidity()
    
        const reader = new FileReader();
        reader.onload = () => {
          this.filePath = reader.result as string;
          this.freemealsImg = reader.result as string;
        }
        reader.readAsDataURL(file)

        this.processing = false;
      }, 1000);
    }
  }


  drawComplete() {
    //console.log(this.signaturePad.toDataURL());
  }

  drawStart() {
    //console.log('begin drawing');
  }

  clearSignature() {
    // turn padd bak on turn off sig
    this.signaturePad.clear();
    //this.sig = false;
    //this.signatureImg = null;
    this.signatureImg2 = false;
  }

  savePad() {
    // turn off pad turn on sig
    //*ngIf="condition"
    this.sig = false;
    const base64Data = this.signaturePad.toDataURL();
    this.signatureImg = base64Data;
    this.signatureImg2 = true;
  }

  get personal() {
    //1
    return this.personalDetails.controls;
  }

  get datesAttending() {
    //2
    return this.datesAttendingDetails.controls;
  }

  get parentCarer() {
    //3
    return this.parentCarerDetails.controls;
  }

  get contact() {
    //4
    return this.contactDetails.controls;
  }

  get altContact() {
    //////////
    return this.altContactDetails.controls;
  }

  get medical() {
    //5
    return this.medicalDetails.controls;
  }

  get dietry() {
    //6
    return this.dietryDetails.controls;
  }

  
  get access() {
    //7
    return this.accessDetails.controls;
  }

  get permission() {
    //8
    return this.permissionDetails.controls;
  }

  get signature() {
    //9
    // this.signaturePad is now available
    this.signaturePad.set('minWidth', 2);
    this.signaturePad.clear();
    return this.signatureDetails.controls;
  }

  next() {
    if (this.step == 1) {
      this.personal_step = true;
      if (this.personalDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 2) {
      this.datesAttending_step = true;
      if (this.datesAttendingDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 3) {
      this.parentCarer_step = true;
      if (this.parentCarerDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 4) {
      this.contact_step = true;
      if (this.contactDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    /*
    if (this.step == 5) {
      this.altContact_step = true;
      if (this.altContactDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    */
    
    if (this.step == 5) {
      this.medical_step = true;
      if (this.medicalDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    
    if (this.step == 6) {
      this.dietry_step = true;
      if (this.dietryDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 7) {
      this.access_step = true;
      if (this.accessDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
    if (this.step == 8) {
      this.permission_step = true;
      if (this.permissionDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }

    if (this.step == 9) {
      this.signature_step = true;
      if (this.signatureDetails.invalid) {
        return;
      }
      this.step++;
      return;
    }
  }
  previous() {
    this.step--;
    if (this.step == 1) {
      this.personal_step = false;
    }
    if (this.step == 2) {
      this.datesAttending_step = false;
    }
    if (this.step == 3) {
      this.parentCarer_step = false;
    }
    if (this.step == 4) {
      this.contact_step = false;
    }
    /*
    if (this.step == 5) {
      this.altContact_step = false;
    }
    */
    if (this.step == 5) {
      this.medical_step = false;
    }
    if (this.step == 6) {
      this.dietry_step = false;
    }
    if (this.step == 7) {
      this.access_step = false;
    }    
    if (this.step == 8) {
      this.permission_step = false;
      this.signatureImg2 = false;
    }  
  }
  submit() {
    if (this.step == 9) {
      this.signature_step = true;
      if (this.signatureDetails.invalid) {
        return;
      }
    }
  }
  onSubmit(): void {
    // collate all th formgroups data into one object
    this.applicationData = {
      ...this.personalDetails.value,
      ...this.datesAttendingDetails.value,
      ...this.parentCarerDetails.value,
      ...this.contactDetails.value,
      //...this.altContactDetails.value,
      ...this.medicalDetails.value,
      ...this.dietryDetails.value,
      ...this.accessDetails.value,
      ...this.permissionDetails.value,
      
      ...this.signatureDetails.value,
    };
    this.contactServices.conactForm(this.applicationData, this.signaturePad, this.freemealsImg);
    //console.warn('Your order has been submitted', this.applicationData);
    alert('your application has been submitted');

    // reset all of the formgroups
    this.personalDetails.reset();
    this.datesAttendingDetails.reset();
    this.parentCarerDetails.reset();
    this.contactDetails.reset();
    //this.altContactDetails.reset();
    this.medicalDetails.reset();
    this.dietryDetails.reset();
    this.accessDetails.reset();
    this.permissionDetails.reset();
    this.signatureDetails.reset();
    this.step = 1;
  }
}
