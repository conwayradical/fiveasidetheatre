import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { StepModel } from '../../models/step.model';

@Component({
  selector: 'app-step-template',
  templateUrl: './step-template.component.html',
  styleUrls: ['./step-template.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StepTemplateComponent implements OnInit {

  @Input() step: StepModel;
  formGroup!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {


    this.formGroup = this.formBuilder.group({
      // personal
      name: [""],
      dob: [""],
      email: [""],
      address: [""],

      // dates attending
      alldates: [""],
      monday16th: [""],
      tuesday17th: [""],
      wednesday18th: [""],
      thursday19th: [""],
      frisday20th: [""],

      // parent / carers details
      parentCarerName:[""],
      relationshipToYP:[""],
      parentCarerAddress:[""],
      parentCarerHomeTel:[""],
      parentCarerMobileTel:[""],
      parentCarerEmail:[""],

      // alt contact details
      altCarerHomeTel:[""],
      altCarerMobileTel:[""],
      altCarerEmail:[""],

      // medical details
      asthmaBrochitis:[""],
      allergies:[""],
      ADHD:[""],
      Diabetes:[""],
      HCFFB:[""],
      hayfever:[""],
      headaches:[""],
      currentMT:[""],
      other:[""],

      // additional details
      diet:[""],
      disibility:[""],
      familyDoctor:[""],
      familyDoctorTel:[""],
      freeMealEvidence:[""], //upload

      // signatures
      signature:[""],

      /*instagram: ["", Validators.required],
      username: ["", Validators.required],
      web: [""],
      twitter: [""],
      profilePicture: ["", Validators.required],
      bio: ["", Validators.required],
      password: ["", [Validators.required, Validators.minLength(8), speshCharValidator]],
      passwordRepeat: ["", Validators.required],
      genres: [[]],
      radiotext:[""],
      podcaststext:[""],
      clubstext:[""],
      othertext:[""],
      radioStations: this.formBuilder.array([]),
      clubs: this.formBuilder.array([]),
      other: this.formBuilder.array([]),
      podcasts: this.formBuilder.array([]),
      package: [""],
      sortCode:[""],
      accountNumber:[""],
      inviteCode:[""]*/
    });


  }

  onCompleteStep() {
    this.step.isComplete = true;
  }

}
