import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { Home2Component } from './pages/home2/home2.component';
import { ThemesComponent } from './pages/themes/themes.component';
import { TrainerComponent } from './pages/trainer/trainer.component';
import { TournamentComponent } from './pages/tournament/tournament.component';
import { ThePeoplesWritesComponent } from './pages/the-peoples-writes/the-peoples-writes.component';
import { AboutusComponent } from './pages/aboutus/aboutus.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: Home2Component },
  { path: 'themes', component: ThemesComponent },
  { path: 'trainer', component: TrainerComponent },
  { path: 'tournament', component: TournamentComponent },
  { path: 'the-peoples-writes', component: ThePeoplesWritesComponent },
  { path: 'aboutus', component: AboutusComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
