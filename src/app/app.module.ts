import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { environment } from 'src/environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { SwiperModule } from 'swiper/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DpDatePickerModule} from 'ng2-date-picker';
import { SignaturePadModule } from 'angular2-signaturepad';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ThemesComponent } from './pages/themes/themes.component';
import { FormPageComponent } from './pages/form-page/form-page.component';
import { StepsComponent } from './components/steps/steps.component';
import { StepTemplateComponent } from './components/step-template/step-template.component';
import { CompletePageComponent } from './pages/complete-page/complete-page.component';
import { TrainerComponent } from './pages/trainer/trainer.component';
import { TournamentComponent } from './pages/tournament/tournament.component';
import { ThePeoplesWritesComponent } from './pages/the-peoples-writes/the-peoples-writes.component';
import { Home2Component } from './pages/home2/home2.component';
import { AboutusComponent } from './pages/aboutus/aboutus.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    FooterComponent,
    ThemesComponent,
    FormPageComponent,
    StepsComponent,
    StepTemplateComponent,
    CompletePageComponent,
    TrainerComponent,
    TournamentComponent,
    ThePeoplesWritesComponent,
    Home2Component,
    AboutusComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    SwiperModule,
    DpDatePickerModule,
    AngularFireModule.initializeApp(environment.firebase),    
    AngularFireAuthModule,
    AngularFirestoreModule,
    SignaturePadModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
