import { Injectable, Inject } from '@angular/core';
import {
  AngularFirestoreCollection,
  AngularFirestoreDocument,
  AngularFirestore,
} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  constructor(public afs: AngularFirestore) {}

  conactForm(data: any, signature: any, freemeals: any) {
    const base64 = signature.toDataURL().split(',')[1];
    let freemealsbase64 = null;
    if(freemeals !== undefined) {
      freemealsbase64 = freemeals.split(',')[1];
    }
    
    let dataList = '<ul>';

    // Extract value from table header.

    for (var key in data) {
      if (data.hasOwnProperty(key)) {
        //console.log(key + ' -> ' + data[key]);
        if(key === "img") {
          //console.log('data[key]', data[key])
        } else {
          dataList += `<li><strong>${key}</strong> : ${data[key]}</li>`;
        }
        
      }
    }
    dataList += '</ul>';

    // fiveasidetheatre@gmail.com
    this.afs
      .collection('mail')
      .add({
        to: 'fiveasidetheatre@gmail.com',
        message: {
          subject: 'Message from FiveASide Theatre Applications',
          text: `From: ${data.name}\n Email: ${
            data.email
          }\n Message: ${JSON.stringify(data)}`,
          html: `From: ${data.name}<br/>Email: ${data.email}<br/>Message: ${dataList}`,
          attachments: [
            {
              // encoded string as an attachment
              filename: data.parentCarerName+'_signature.png',
              content: base64,
              encoding: 'base64',
            },
            {
              filename: data.parentCarerName+'_freeMealsEvidence.png',
              content: freemealsbase64,
              encoding: 'base64',
            }
          ],
        },
      })
      .then(() => {
        console.log('Queued email for delivery!');
      });
  }
}
