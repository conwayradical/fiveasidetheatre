// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyA2j8is9x9vgSh6Tyc4NPVkOkSoJFslJJ4",
    authDomain: "five-a-side-theatre.firebaseapp.com",
    projectId: "five-a-side-theatre",
    storageBucket: "five-a-side-theatre.appspot.com",
    messagingSenderId: "288507195226",
    appId: "1:288507195226:web:f30c7777463f778094cd63"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
